"use strict";

import React, { makeId } from 'react-fastgui';
import processClassNameAndStyle from './processClassNameAndStyle';

export default function *list(props, children){
    // console.info("list", props, children);
    // const id = makeId(props);
    yield (<ul { ...props } { ...processClassNameAndStyle(props) }>{
        children.map((item, i) => <li key={i}>{ item }</li>)
    }</ul>);
}

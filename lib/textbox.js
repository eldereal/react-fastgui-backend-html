"use strict";

import React from 'react-fastgui';
import processClassNameAndStyle from './processClassNameAndStyle';

export default function* TextBox(props, children){
    let text = this.persist("text") || "";
    // console.info("persist text", text);
    const evtText = this.transient("text");
    // console.info("transient text", evtText);
    const newText = evtText !== undefined ? evtText : props.value;
    if(newText !== undefined && newText !== text){
        text = newText;
        this.persist("text", text);
    }
    yield <input { ...props } { ...processClassNameAndStyle(props) }
        type="text" value={ text } onChange={(evt)=>{
            // console.info("update text", evt.target.value);
            const text = evt.target.value;
            this.transient("text", text);
            this.persist("text", text);
            this.reload();
        }}/>;
    // console.info("component", component);
    return text;
}

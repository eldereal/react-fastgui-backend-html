"use strict";

import React from 'react';
import { render } from 'react-dom';
import { GUIComponent, Dimensions } from 'react-fastgui';



export default function(ongui, options){

    Dimensions.set({window: {
        get width(){
            return window.innerWidth;
        },
        get height(){
            return window.innerHeight;
        }
    }});
    this.navigator.init();
    this.navigator.push(ongui);
}

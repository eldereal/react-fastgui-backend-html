"use strict";
/*jshint scripturl:true*/

import React from 'react-fastgui';
import processClassNameAndStyle from './processClassNameAndStyle';

export default function*(props, children){
    yield <div
        onClick={ () => {
            // console.info(this);
            this.transient("pressed", true);
            this.reload();
        }}
        { ...props }
        { ...processClassNameAndStyle(props) }
        >
        { children }
    </div>;
    return this.transient("pressed");
}

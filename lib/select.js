"use strict";

import React, { createComponent, PseudoComponent } from 'react-fastgui';
import processClassNameAndStyle from './processClassNameAndStyle';
import hash from 'object-hash';

const styles = {
    label: {
        display: 'block',
        position: 'relative',
        overflow: 'hidden'
    },
    select: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        opacity: 0,
        width: '100%',
        height: '100%',
    }
};

function *Select(props, children){
    const options = children
        .filter(item => item.type === "option")
        .map(item => ({
            value: item.props.value === undefined ? null : item.props.value,
            label: item.props.children && item.props.children.join && item.props.children.join()
        }));
    options.forEach(item => item.key = item.value && item.value.key || hash(item.value));

    let selectedKey = this.persist("selected") || options[0] && options[0].key;

    const eventSelectedKey = this.transient("selected") || props && props.value && props.value.key || hash(props.value);

    if(eventSelectedKey !== undefined && eventSelectedKey !== selectedKey){
        selectedKey = eventSelectedKey;
        this.persist("selected", selectedKey);
    }

    let selectedLabel = "";
    let selectedValue;
    for(const opt of options){
        if (opt.key === selectedKey) {
            selectedLabel = opt.label;
            selectedValue = opt.value;
        }
    }
    yield <label { ...props } { ...processClassNameAndStyle(props, { style: styles.label })}>
        { selectedLabel }
        <select style={ styles.select } value={ selectedKey } onChange={(evt)=>{
            // console.info(evt.target.value);
            this.persist("selected", evt.target.value);
            this.transient("selected", evt.target.value);
            this.reload();
        }}>{
            options.map(item=>
                <option key={ item.key } value={ item.key }>{ item.label }</option>
            ) }
        </select>
    </label>;
    return selectedValue;
}
function* Option(props, children){
    yield <option key={ props && props.value && props.value.key || hash( props.value ) } { ...props } { ...processClassNameAndStyle(props) }>{ children }</option>;
}

const comp = createComponent(Select);
comp.Option = createComponent(Option);
export default comp;

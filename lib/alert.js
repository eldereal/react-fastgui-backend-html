"use strict";

import React, { createComponent, createPseudoComponent } from 'react-fastgui';
import hash from 'object-hash';
import RenderInBody from './RenderInBody';
import blacklist from 'blacklist';
import processClassNameAndStyle from './processClassNameAndStyle';

const comp = createComponent(Alert);
comp.Option = createPseudoComponent();
export default comp;

function *Alert(props, children){
    const opts = [];
    const actChildren = [];
    children.forEach(item => {
        if(comp.Option.isPrototypeOf(item)){
            opts.push({
                props: item.props,
                children: item.children,
                value: item.props && item.props.value
            });
        }else{
            actChildren.push(item);
        }
    });

    // console.info("alert", opts, actChildren);
    yield <RenderInBody __source={ props && props.__source } __hash={ props && props.__hash } key={ props && props.key }>
        <div className="rf-mask rf-centering">
            <div
                { ...blacklist(props, "__source", "__hash", "key", "className", "style") }
                { ...processClassNameAndStyle(props, { className: "rf-alert" }) }
                >
                { React.createElement("div", { className: "rf-alert-body" }, ...actChildren )}
                <div className="rf-alert-footer">
                    { opts.map((opt,i)=> <div
                        { ...blacklist(opt.props, "value", "className", "style") }
                        { ...processClassNameAndStyle(opt.props, { className: "rf-alert-option rf-centering" }) }
                        onClick={()=>{
                            // console.info("click opt", opt);
                            this.transient("result", opt.value);
                            this.reload();
                        }}
                        >
                            { opt.children }
                        </div>)
                    }
                </div>
            </div>
        </div>
    </RenderInBody>;
    // console.info("alert result", this.transient("result"));
    return this.transient("result");
}

"use strict";
/*jshint scripturl:true*/

import React, { createComponent } from 'react-fastgui';
import processClassNameAndStyle from './processClassNameAndStyle';
import mergeStyle from './mergeStyle';
import blacklist from 'blacklist';
import classnames from 'classnames';
// import hash from 'object-hash';

function* ToggleButton(oprops, children){

    // console.info("ToggleButton", oprops, children);

    let checked = this.persist("checked");
    if(oprops && oprops.checked !== undefined){
        checked = Boolean(oprops.checked);
    }

    const eventChecked = this.transient("checked");
    if(eventChecked !== undefined){
        checked = eventChecked;
    }
    if(this.persist("checked") !== checked){
        this.persist("checked", checked);
    }

    const props = blacklist(oprops, "style", "onStyle", "offStyle", "checked");
    let style = oprops && oprops.style || [];
    if(!Array.isArray(style)){
        style = [style];
    }
    let overrideStyle = oprops && (checked ? oprops.onStyle : oprops.offStyle) || [];
    if(!Array.isArray(overrideStyle)){
        overrideStyle = [overrideStyle];
    }
    style = style.concat(overrideStyle);

    yield <div { ...props } { ...processClassNameAndStyle(props) }
        style={ mergeStyle(...style) }
        onClick={ () => {
            const value = !this.persist("checked");
            this.transient("checked", value);
            this.reload();
        }} >
        { children }
    </div>;

    const group = this.components["ToggleButton.Group"];
    if(group){
        group.registerToggleButton(this.ref, checked, eventChecked, props.value);
    }

    return checked;
}

ToggleButton.componentName = "ToggleButton";

ToggleButton.contextRef = function(obj){

    // console.info("ToggleButton.ref", this);
    Object.defineProperty(obj, "checked", {
        get: () => {
            const transient = this.transient("checked");
            return transient === undefined ? this.persist("checked") : transient;
        },
        set: (value) => {
            this.transient("checked", Boolean(value));
            this.reload();
        }
    });
    Object.defineProperty(obj, "value", {
        get: () => {
            const transient = this.transient("checked");
            return transient === undefined ? this.persist("checked") : transient;
        }
    });
};

function* Group(props, children){
    yield <div {...processProps(props)}>
        { children }
    </div>;
    console.info("ToggleButton.Group", props, children);
}

Group.componentName = "ToggleButton.Group";

Group.contextRef = function(obj){

    // console.info("ToggleButton.Group.ref", this);
    obj.buttons = [];
};

const module = createComponent(ToggleButton);
module.Group = createComponent(Group);
export default module;

"use strict";

import { StyleSheet } from 'react-fastgui';
import decamelize from 'decamelize';
import cx from 'classnames';

const { isPersistedStyleSheet, getPersistId, setPersistData, getPersistData } = StyleSheet;

const globalStyles = [ "" ]; //skip zero index
const globalStyleIndex = {};

let stylesheet;

const styleToStringHandler = {

};

function defaultStyleToStringHandler(styleObj, key, value){
    const name = decamelize(key, "-");
    if(typeof value === "number"){
        return name + ":" + value + "px;";
    }else{
        return name + ":" + value + ";";
    }
}

function styleToString(className, obj){
    let style = '.' + className + "{";
    for(const key in obj){
        style += (styleToStringHandler[key] || defaultStyleToStringHandler)(obj, key, obj[key]);
    }
    style += "}";
    return style;
}

function processStyleObject(style){
    for(const name in style){
        const value = style[name];
        if(typeof value === "number"){
            style[name] = `${ value }px`;
        }
    }
    return style;
}

function createStyleSheet(){
    const styleEl = document.createElement('style');
    document.head.appendChild(styleEl);

    let stylesheet = styleEl.sheet;
    return stylesheet;
}

export default function processClassNameAndStyle(...propslist){
    let classNames;
    let style;
    for(const props of propslist){
        if(props.style){
            const istyles = Array.isArray(props.style) ? props.style : [props.style];
            let currentMaxIdx = 0;
            for(const styleObj of istyles){
                if(!styleObj) continue;
                if(isPersistedStyleSheet(styleObj)){
                    const id = getPersistId(styleObj);
                    if(!globalStyleIndex[id]){
                        globalStyleIndex[id] = globalStyles.length;
                        globalStyles.push(styleObj);
                        if(!stylesheet) {
                            stylesheet = createStyleSheet();
                        }
                        stylesheet.insertRule(styleToString(`_rfst${ globalStyleIndex[id] }`, styleObj), stylesheet.cssRules.length);
                    }
                    const idx = globalStyleIndex[id];
                    if (currentMaxIdx <= idx) { //normal order, render as css class
                        currentMaxIdx = idx;

                        if(!classNames){
                            classNames = `_rfst${ globalStyleIndex[id] }`;
                        } else {
                            classNames += ` _rfst${ globalStyleIndex[id] }`;
                        }
                        if(style){
                            for(const key in style){
                                if(styleObj[key] !== undefined && styleObj[key] !== null){
                                    style[key] = styleObj[key];
                                }
                            }
                        }
                    } else { //reverse order, convert css class to inline style to correctly override styles
                        if(!style){
                            style = {};
                        }
                        for(const key in styleObj){
                            style[key] = styleObj[key];
                        }
                    }
                }else{
                    if(!style){
                        style = {};
                    }
                    for(const key in styleObj){
                        style[key] = styleObj[key];
                    }
                }
            }
        }
        if(props.className){
            classNames = cx(classNames, props.className);
        }
    }
    return { className: classNames, style: processStyleObject(style) };
}

"use strict";

import React, {
    Component,
    PropTypes
} from "react";
import blacklist from 'blacklist';

export default class Mountable extends Component {

    componentDidMount() {
        if(this.props.onMount){
            this.props.onMount(this.refs.mount);
        }
    }

    render() {
        const props = blacklist(this.props, "tag");
        return <this.props.tag { ...props } ref="mount">
            { this.props.children }
        </this.props.tag>;
    }
}

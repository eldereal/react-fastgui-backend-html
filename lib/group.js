"use strict";

import React from 'react-fastgui';
import processClassNameAndStyle from './processClassNameAndStyle';

export default function *group(props, children){
    yield <div {...props} {...processClassNameAndStyle(props)}>
        { children }
    </div>;
}

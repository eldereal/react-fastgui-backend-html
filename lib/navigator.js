"use strict";

import React, { GUIComponent } from 'react-fastgui';
import { render, unmountComponentAtNode } from 'react-dom';

const viewstack = [];
let currentTop = -1;

class NavStackItem {

    constructor(name, dom, ongui, component, options){
        this.name = name;
        this.dom = dom;
        this.ongui = ongui;
        this.component = component;
        this.options = options;
    }

    hide(){
        this.dom.className = "rf-root rf-hide";
        if(this.options && this.options.onHide){
            this.options.onHide.call(this, this.result);
        }
    }

    show(){
        this.dom.className = "rf-root";
    }

    destroy(){
        if(this.options && this.options.onDestroy){
            this.options.onDestroy.call(this, this.result);
        }
        // this.component.dispose();
        unmountComponentAtNode(this.dom);
        this.dom.parentNode.removeChild(this.dom);
    }
}

export default class Navigator {

    constructor(backend){
        this.backend = backend;
    }

    init(){
        window.onhashchange = this.hashchange.bind(this);
    }

    hashchange(){
        let hash = location.hash;
        if(hash[0] === '#'){
            hash = hash.substr(1);
        }
        const stackmap = hash.split('/');
        // console.info("hash change", stackmap, viewstack);
        let i = 0;
        for(; i<viewstack.length && i < stackmap.length; i++){
            const match = !stackmap[i] || viewstack[i].name == stackmap[i];
            if(!match){
                break;
            }
        }
        currentTop = i - 1;
        for(i in viewstack){
            if(i == currentTop){
                // console.info("show", i, viewstack[i]);
                viewstack[i].show();
            }else{
                // console.info("hide", i, viewstack[i]);
                viewstack[i].hide();
            }
        }
        location.hash = viewstack.slice(0, currentTop + 1).map(item => item.name).join("/");
    }

    replace(item){
        const top = viewstack[currentTop];
        if(top){
            top.destroy();
        }

        const name = item.displayName || item.name;
        const id = name + "-" + Math.random().toString(36).substr(-6);

        const root = document.createElement("div");
        root.id = id;
        root.className = "rf-root";
        document.getElementsByTagName("body")[0].appendChild(root);

        const comp = <GUIComponent
            id = { id }
            backend = { this.backend }
            ongui = { item }
            className = { "rf-page" }/>;
        render(comp, root);

        viewstack[currentTop] = new NavStackItem(name, root, item, comp);
        location.hash = viewstack.map(item => item.name).join("/");
    }

    push(item){
        return new Promise((fulfill, reject)=>{
            const name = item.displayName || item.name;
            console.info("push navigator", name, this);

            const top = viewstack[currentTop];
            if(top){
                top.hide();
            }
            const id = name + "-" + Math.random().toString(36).substr(-6);

            const root = document.createElement("div");
            root.id = id;
            root.className = "rf-root";
            document.getElementsByTagName("body")[0].appendChild(root);

            const comp = <GUIComponent
                id = { id }
                backend = { this.backend }
                ongui = { item }
                className = { "rf-page" }/>;
            render(comp, root);

            currentTop ++;
            viewstack.splice(currentTop).forEach(item => item.destroy());
            viewstack.push(new NavStackItem(name, root, item, comp, {
                onDestroy: result => fulfill(result),
                onHide: result => fulfill(result) }));
            location.hash = viewstack.map(item => item.name).join("/");
        });
    }

    pop(result){
        if(currentTop === 0){//cannot pop last view
            return;
        }
        console.info("pop", viewstack[currentTop], result);
        viewstack[currentTop].result = result;
        viewstack.splice(currentTop).forEach(item => item.destroy());
        currentTop --;
        viewstack[currentTop].show();
        location.hash = viewstack.map(item => item.name).join("/");
    }

    canPop(){
        return currentTop > 0;
    }
}

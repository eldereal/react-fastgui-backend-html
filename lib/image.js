"use strict";

import React from 'react-fastgui';
import processClassNameAndStyle from './processClassNameAndStyle';
import blacklist from 'blacklist';

export default function*(props, children){

    let image;
    if(props.source){
        if(props.source.scales){
            image = selectPixelRatio(props.source);
        }else{
            image = props.source;
        }
    }
    const overrideStyle = {
        background: `url(${image && image.src || image.uri}) no-repeat`,
    };

    const nonOverrideStyle = {
        width: image && image.width &&(image.width / (image.scale || 1)),
        height: image && image.height && (image.height / (image.scale || 1))
    };

    const resizeMode = props && props.resizeMode || 'cover';
    if(resizeMode === 'contain'){
        overrideStyle.backgroundSize = 'contain';
        overrideStyle.backgroundPosition = 'center center';
    }else if(resizeMode === 'cover'){
        overrideStyle.backgroundSize = 'cover';
        overrideStyle.backgroundPosition = 'center center';
    }else if(resizeMode === 'stretch'){
        overrideStyle.backgroundSize = '100% 100%';
        overrideStyle.backgroundPosition = '0 0';
    }else{
        console.warn(`react-fastgui-backend-html doesn't support Image.resizeMode="${props.resizeMode}"`);
    }
    yield <div
            { ...blacklist(props, "source") }
            { ...processClassNameAndStyle({ style: nonOverrideStyle }, props, { style: overrideStyle} ) }>
            { children }
        </div>;
}

function selectPixelRatio(image){
    if(typeof window !== 'undefined'){
        var pixelRatio = window.devicePixelRatio || 1;
        var bestError;
        var bestChoice;
        for(var scale in image.scales){
            var error = Number(scale) / pixelRatio;
            if(error < 1){
                error = 1/error;
            }
            if(!(bestError < error)){
                bestError = error;
                bestChoice = image.scales[scale];
            }
        }
        return bestChoice;
    }else{
        return image.scales["1"] || image.scales["2"] || image.scales[3];
    }
}

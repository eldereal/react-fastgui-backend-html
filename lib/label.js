"use strict";

import React from 'react-fastgui';
import processClassNameAndStyle from './processClassNameAndStyle';

export default function *label(props, children){
    yield (<span {...props} {...processClassNameAndStyle(props)}>
        { children }
    </span>);
}

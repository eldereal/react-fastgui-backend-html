"use strict";

import React, { createComponent } from 'react-fastgui';
import processClassNameAndStyle from './processClassNameAndStyle';
import blacklist from 'blacklist';
import classnames from 'classnames';
import Mountable from './Mountable';

import '../resources/stylesheets/scrollpane.scss';

function *scrollpane(oprops, children){
    const ostyle = {};
    const dir = oprops && oprops.direction;
    if(dir === "horizontal"){
        ostyle.overflowX="auto";
    }else if(dir === "vertical"){
        ostyle.overflowY="auto";
    }else{
        ostyle.overflow="auto";
    }
    const props = blacklist(oprops, "direction", "className", "initialPosition");

    yield <Mountable tag="div"
        onMount={(element)=>{
            if(oprops.initialPosition === "end"){
                if(dir === "horizontal"){
                    element.scrollLeft = element.scrollWidth - element.clientWidth;
                }else if(dir === "vertical"){
                    element.scrollTop = element.scrollHeight - element.clientHeight;
                }else{
                    element.scrollLeft = element.scrollWidth - element.clientWidth;
                    element.scrollTop = element.scrollHeight - element.clientHeight;
                }
            }
        }}
        { ...props }
        { ...processClassNameAndStyle(oprops, { style: ostyle, className: "rf-scrollpane" }) }
        >
        { children }
    </Mountable>;
}

export default createComponent(scrollpane);

"use strict";

import React, { createComponent } from 'react-fastgui';

import button from './button';
import page from './page';
import group from './group';
import label from './label';
import list from './list';
import textbox from './textbox';
import bootstrap from './bootstrap';
import image from './image';
import scrollpane from './scrollpane';
import select from './select';
import alert from './alert';
import Navigator from './navigator';

import '../resources/stylesheets/common.scss';
import '../resources/stylesheets/alert.scss';

const backend = {
    components: {
        Button: createComponent(button),
        // ToggleButton: toggleButton,
        // "ToggleButton.Group": toggleButton.Group,
        Page: createComponent(page),
        Group: createComponent(group),
        Label: createComponent(label),
        List: createComponent(list),
        TextBox: createComponent(textbox),
        Image: createComponent(image),
        ScrollPane: scrollpane,
        Select: select,
        "Select.Option": select.Option,
        Alert: alert,
        "Alert.Option": alert.Option,
    },
    rawText: function(...textlist){
        return textlist;
    },
    bootstrap: bootstrap
};

backend.navigator = new Navigator(backend);

export default backend;
export const Button = backend.components.Button;
export const Group = backend.components.Group;
export const Label = backend.components.Label;
export const List = backend.components.List;
export const Image = backend.components.Image;
// console.info("export const Select", backend.components.Select);
export const Select = backend.components.Select;
